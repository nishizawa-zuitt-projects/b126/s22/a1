/* Tyepe all commands in this mongo.js file before copy/paste them into Robo3T

Inside of b126_chatapp, do the following:

	1. Inside the channels collection and inside of that, create 5 different documents each with the following info:
		-"channelName": ("Red Team", "Blue Team", "Yellow Team", "Black Team", "White Team")
		-"members": Any 3 names as strings
	2. Add an "isActive" property to all channles with a value of true (boolean)
	3. Change the Yellow and Black team to inactive
	4. Remove the white Team channel
	5. Find all active channels
	6. Delete all inactive channels

*/

//1
db.channels.insertMany([
	{
		"channelName": "Red Team",
		"members": ["aaa", "bbb", "ccc"],
	},
	{
		"channelName": "Blue Team",
		"members": ["ddd", "eee", "fff"],
	},
	{
		"channelName": "Yello Team",
		"members": ["ggg", "hhh", "iii"],
	},
	{
		"channelName": "Black Team",
		"members": ["jjj", "kkk", "lll"],
	},
	{
		"channelName": "White Team",
		"members": ["mmm", "nnn", "ooo"],
	},
])

//2
db.channels.updateMany(
	{
	},
	{
		$set:{
			"isActive": true
		}
	}
)

//3
db.channels.updateOne(
	{
		"_id": ObjectId("6172acc092c6672e0cadf3cc")
	},
	{
		$set:{
			"isActive": false
		}
	}

)


db.channels.updateOne(
	{
		"_id": ObjectId("6172acc092c6672e0cadf3cd")
	},
	{
		$set:{
			"isActive": false
		}
	}

)

//4
db.channels.deleteOne({
	"_id" : ObjectId("6172acc092c6672e0cadf3ce")
})

//5
db.channels.find({
	"isActive": true
})

//6
db.channels.deleteMany({
	"isActive": false
})




